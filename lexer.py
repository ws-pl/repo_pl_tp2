#lexer.py
import ply.lex as lex

class CSVLexer:
    tokens = ('COLUMN','NEWLINE',"QUOTEDCOLUMN")
    states = (("insideline","exclusive"),)
    
    t_ignore = '\n'
    t_ANY_ignore = ','
    
    
    def _update_table(self, value):
        
        
        if self._start:
            self.table[value] = []
        else:
            keys = list(self.table.keys())
            self.table[keys[self.index]].append(value)
            self.index += 1
        
    def __init__(self):
        self.lexer = None
        self.table = {}
        self._start = True
        self.index = 0
    
    
    def t_comment(self, t):
        r"\#[^\n]+\n"
        pass
    def t_QUOTEDCOLUMN(self,t):
        r'"[^"]+"'
        t.value = t.value[1:-1]
        self._update_table(t.value)
        t.lexer.begin("insideline")
        return t
    
    def t_COLUMN(self, t):
        r"[^,\n]+"
        self._update_table(t.value)
        t.lexer.begin("insideline")
        return t
    
    def t_insideline_QUOTEDCOLUMN(self, t):
        r'"[^"]+"'
        t.value = t.value[1:-1]
        self._update_table(t.value)
    def t_insideline_COLUMN(self, t):
        r"[^,\n]+"
        self._update_table(t.value)
        t.lexer.begin("insideline")
        return t
        
    def t_insideline_NEWLINE(self, t):
        r'\n'
        if self._start:
            self._start = False
        else:
            self.index = 0
        t.lexer.begin("INITIAL")
    
    def t_insideline_error(self, t):
        print(f"Unrecognized token: {t.value[:10]}")
        print("Exiting!!")
        exit(1)
    
    def t_error(self, t):
        print(f"Unrecognized token: {t.value[:10]}")
        print("Exiting!!")
        exit(1)
    def build(self, **kwargs):
        self.lexer = lex.lex(self, kwargs)
    def input(self, string):
        self.lexer.input(string)
        for token in iter(self.lexer.token,None):
            pass
        print(self.table) 
        print("Finished!!")
    
        
        
        