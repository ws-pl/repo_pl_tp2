def slurp(filename):
    try:
        with open(filename) as fh:
            content = fh.read()
    except:
        raise FileNotFoundError('File not found')
    return content
        