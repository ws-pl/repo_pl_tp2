#main.py
from utils import slurp
from lexer import CSVLexer

csv = slurp('produtos.csv')

csv_lexer = CSVLexer()
csv_lexer.build()
csv_lexer.input(csv)

